#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>

sem_t mutex;
sem_t wrt;
int readcount=0;

void *Reader(void *a);
void *Writer(void *a);

int main(){
	int i=0,Num_Red=0,Num_W;

	pthread_t Re_thr[100],Wr_thrr[100];
	printf("\nnumber of Readers thread:");
	scanf("%d",&Num_Red);
	printf("\nnumber of Writers thread:");
	scanf("%d",&Num_W);
	for(i=0;i<Num_Red;i++)
	{
	pthread_create(&Re_thr[i],NULL,Reader,(void *)i);
	}
	for(i=0;i<Num_Red;i++)
	{
	pthread_create(&Wr_thrr[i],NULL,Writer,(void *)i);
	}
	for(i=0;i<Num_W;i++)
	{
	pthread_join(Wr_thrr[i],NULL);
	}
	
	for(i=0;i<Num_Red;i++)
	{
	pthread_join(Re_thr[i],NULL);
	}
	return 0;
}

void * Writer(void *arg){
	FILE *fptr;
	int temp=(intptr_t)arg;
	sem_wait(&wrt);
	printf("\nWriter %d is writing",temp+1);
	fptr=fopen("C:\\Users\\VAHED\\Desktop\\1\\vahedtxt","a");
	fprintf(fptr,"Writer %d is writing",temp);
	fclose(fptr);
	sem_post(&wrt);
}
void *Reader(void *arg){
	FILE *fptr;
	int temp=(intptr_t)arg;
	sem_wait(&mutex);
	readcount++;
	if(readcount==1)
		sem_wait(&wrt);
	sem_post(&mutex);
	printf("\nReader %d is Reading",temp+1);
	fptr=fopen("C:\\Users\\VAHED\\Desktop\\1\\vahedtxt","w");
	fprintf(fptr,"Reader %d is Reading",temp);
	fclose(fptr);
	sem_wait(&mutex);
	readcount--;
	if(readcount==0)
		sem_post(&wrt);
	sem_post(&mutex);
}
